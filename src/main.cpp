#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D1 //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
int cpt = 1;

void allumerLeds(int prmNumPrincipal){
  FastLED.clear();
  leds[((prmNumPrincipal - 1)+10)%NUM_LEDS] = CHSV(155, 255, 55); //Allumer la led
  leds[prmNumPrincipal] = CHSV(155, 255, 255);     //Allumer la led
  leds[((prmNumPrincipal + 1)+10)%NUM_LEDS] = CHSV(155, 255, 55); //Allumer la led
  FastLED.show();                      //appliquer les modification apporter
}

void setup()
{
  //configuration du ruban de LEDs
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop()
{
  allumerLeds(cpt);
  delay(150);
  cpt++;                               //Compteur en incrementation
  //Reset la position des Leds selon la condition de la position actuelle
  if (cpt >= 10)
  {    cpt = 0;  }
}