#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D1 //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];

void setup()
{
  //configuration du ruban de LEDs
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}
int cpt1 = 1, cpt2 = 2, cpt3 = 3;

void loop()
{
  leds[cpt1] = CHSV(155, 255, 127); //Allumer la led
  leds[cpt2] = CHSV(155, 255, 255);     //Allumer la led
  leds[cpt3] = CHSV(155, 255, 127); //Allumer la led
  FastLED.show();                      //appliquer les modification apporter
  delay(150);                           //un delai de 60ms
  leds[cpt1] = CRGB::Black;         //Allumer la led
  leds[cpt2] = CRGB::Black;             //Eteindre la led
  leds[cpt3] = CRGB::Black;         //Eteindre la led
  FastLED.show();                      //appliquer les modification apporter
  delay(150);                          //un delai de 60ms
  cpt1++;                               //Compteur en incrementation
  cpt2++;                               //Compteur en incrementation
  cpt3++;                               //Compteur en incrementation
  if (cpt1 == 10)
  {
    cpt1 = 0;
  }
  if (cpt2 == 10)
  {
    cpt2 = 0;
  }
  if (cpt3 == 10)
  {
    cpt3 = 0;
  }
}
